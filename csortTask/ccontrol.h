#ifndef CCONTROL_H
#define CCONTROL_H

#include <QObject>
#include <functional>

enum class AccessLevel : size_t
{
    None = 0,
    Operator,
    Technical,
    Engineer,
};

enum class ControlType : size_t
{
    Button = 0,
    Caption,
    ControlTypeConut
};

class CControl : public QObject
{
    Q_OBJECT
public:
    explicit CControl(QObject *parent = nullptr);
    virtual ControlType GetControlType() = 0;
    virtual QString GetCaption() = 0;
    virtual AccessLevel GetAccessLevel() = 0;
protected:
    QString m_caption;
    AccessLevel m_accessLevel;
};

class CCaptionControl : public CControl
{
public:
    explicit CCaptionControl(QString caption, AccessLevel level = AccessLevel::None)
        : m_caption(caption)
        , m_accessLevel(level) {}
    virtual ControlType GetControlType() override { return ControlType::Caption; }
    virtual QString GetCaption() override {return m_caption; }
    virtual AccessLevel GetAccessLevel() { return m_accessLevel; }
private:
    QString m_caption;
    AccessLevel m_accessLevel;
};

class CButtonControl : public CControl
{
public:
    explicit CButtonControl(QString caption, std::function<void()> run, AccessLevel level = AccessLevel::None)
        : m_caption(caption)
        , m_accessLevel(level)
        , m_run(run) {}
    virtual ControlType GetControlType() override { return ControlType::Button; }
    virtual QString GetCaption() override {return m_caption; }
    virtual AccessLevel GetAccessLevel() override { return m_accessLevel; }
    void Exec() {m_run(); }
private:
    QString m_caption;
    std::function<void()> m_run;
    AccessLevel m_accessLevel;
};

#endif // CCONTROL_H
