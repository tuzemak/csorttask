#ifndef CMAINMODEL_H
#define CMAINMODEL_H

#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include <ccontrol.h>
#include <QVector>

enum ControlRole {
    IdRole = Qt::UserRole + 1,
    TypeRole,
    CaptionRole,
    AccessLevelRole,
};
class CMainModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit CMainModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const;
private:
    QVector<CControl *> m_controls;
};

class CAccessLevelProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit CAccessLevelProxyModel(QObject *parent = nullptr);
public slots:
    void setPassword(QString password);
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
private:
    const QHash<QString, AccessLevel> m_passwords;
    AccessLevel m_currentAccessLevel;
};

class CButtonsProxyModel: public QSortFilterProxyModel
{
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
};

class CCaptionsProxyModel: public QSortFilterProxyModel
{
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
};

#endif // CMAINMODEL_H
