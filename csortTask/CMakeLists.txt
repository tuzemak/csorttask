cmake_minimum_required(VERSION 3.5)

project(csortTask LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


find_package(Qt5 COMPONENTS Core Quick REQUIRED)

set(SOURCES main.cpp
            cmainmodel.cpp
            ccontrol.cpp)

set(HEADERS cmainmodel.h
            ccontrol.h)

set(RESOURCES qml.qrc
              captionwindow.qml)

add_executable(csortTask ${SOURCES} ${HEADERS} ${RESOURCES})

target_compile_definitions(csortTask
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(csortTask
  PRIVATE Qt5::Core Qt5::Quick)
