#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <ccontrol.h>
#include <cmainmodel.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    CMainModel mainModel;

    CAccessLevelProxyModel accessLevelModel;
    accessLevelModel.setSourceModel(&mainModel);
    accessLevelModel.setDynamicSortFilter(true);

    CButtonsProxyModel buttonsModel;
    buttonsModel.setSourceModel(&accessLevelModel);
    buttonsModel.setDynamicSortFilter(true);

    CCaptionsProxyModel captionsModel;
    captionsModel.setSourceModel(&accessLevelModel);
    captionsModel.setDynamicSortFilter(true);

    QQmlApplicationEngine engine;
    auto ctx = engine.rootContext();
    ctx->setContextProperty("accessLevelModel", &accessLevelModel);
    ctx->setContextProperty("buttonsModel", &buttonsModel);
    ctx->setContextProperty("captionsModel", &captionsModel);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    QObject *item = engine.rootObjects().first();
    QObject::connect(item, SIGNAL(onSetPassword(QString)), &accessLevelModel, SLOT(setPassword(QString)));

    return app.exec();
}
