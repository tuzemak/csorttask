#include "cmainmodel.h"
#include <QDebug>

CMainModel::CMainModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_controls.append(new CCaptionControl("Caption1", AccessLevel::Operator));
    m_controls.append(new CButtonControl("Button1", []() {qDebug() << "Activate Button 1";}, AccessLevel::Operator));
    m_controls.append(new CCaptionControl("Caption2", AccessLevel::Operator));
    m_controls.append(new CButtonControl("Button2", []() {qDebug() << "Activate Button 2";}, AccessLevel::Operator));
    m_controls.append(new CCaptionControl("Caption3", AccessLevel::Technical));
    m_controls.append(new CButtonControl("Button3", []() {qDebug() << "Activate Button 3";}, AccessLevel::Technical));
    m_controls.append(new CCaptionControl("Caption4", AccessLevel::Technical));
    m_controls.append(new CButtonControl("Button4", []() {qDebug() << "Activate Button 4";}, AccessLevel::Technical));
    m_controls.append(new CCaptionControl("Caption5", AccessLevel::Engineer));
    m_controls.append(new CButtonControl("Button5", []() {qDebug() << "Activate Button 5";}, AccessLevel::Engineer));
    m_controls.append(new CCaptionControl("Caption6", AccessLevel::Engineer));
    m_controls.append(new CButtonControl("Button6", []() {qDebug() << "Activate Button 6";}, AccessLevel::Engineer));
}

int CMainModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_controls.size();
}

QVariant CMainModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    int row = index.row();

    if (role == IdRole)
        return QVariant(row);

    if (role == CaptionRole)
        return QVariant(m_controls[row]->GetCaption());

    if (role == TypeRole)
        return QVariant(int(m_controls[row]->GetControlType()));

    if (role == AccessLevelRole)
        return QVariant(int(m_controls[row]->GetAccessLevel()));

    return QVariant();
}

QHash<int, QByteArray> CMainModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TypeRole] = "type";
    roles[IdRole] = "id";
    roles[CaptionRole] = "caption";
    roles[AccessLevelRole] = "accessLevel";
    return roles;
}

CAccessLevelProxyModel::CAccessLevelProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
    , m_passwords({ {"111", AccessLevel::Technical},
                    {"222", AccessLevel::Engineer} })
    , m_currentAccessLevel(AccessLevel::Operator)
{}

void CAccessLevelProxyModel::setPassword(QString password)
{
    auto it = m_passwords.find(password);
    m_currentAccessLevel = (it == m_passwords.end())?AccessLevel::Operator:*it;
    emit filterChanged();
}

bool CAccessLevelProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    auto index = sourceModel()->index(source_row, 0, source_parent);
    return sourceModel()->data(index, AccessLevelRole) <= QVariant(int(m_currentAccessLevel));
}

bool CButtonsProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    auto index = sourceModel()->index(source_row, 0, source_parent);
    return sourceModel()->data(index, TypeRole) == QVariant(int(ControlType::Button));
}

bool CCaptionsProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    auto index = sourceModel()->index(source_row, 0, source_parent);
    return sourceModel()->data(index, TypeRole) == QVariant(int(ControlType::Caption));
}
