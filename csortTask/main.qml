import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

    signal onSetPassword(string str)

    ScrollView {
        x: 6
        y: 40
        width: parent.width
        ListView {
            id: listView
            model: buttonsModel
            delegate: ItemDelegate {
                width: parent.width
                Button {
                    text: caption;
                    width: parent.width
                }
            }
        }
    }

    Button {
       id: newWindowButton
       x: 0
       y: 432
       width: 369
       height: 48
       text: qsTr("New window")
       onClicked: {
           var component = Qt.createComponent("captionwindow.qml")
           var window    = component.createObject(root)
           window.show()
       }
    }

    Button {
        id: setPasswordButton
        x: 406
        y: 6
        text: qsTr("Set password")
        onClicked: root.onSetPassword(textEdit.text);
    }

    TextInput {
        id: textEdit
        x: 6
        y: 6
        font.pixelSize: 24
        color: "white"
        width: 400
        height: 24
        text: qsTr("password")
        onAccepted: root.onSetPassword(textEdit.text);
    }
}
